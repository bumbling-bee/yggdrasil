import Vue from 'vue'
import home from './home.vue'

var app = new Vue({
    el: '#app',
    data: {},
    components: {
      'home': home
    },
    render() {
      return <home></home>
    }
})