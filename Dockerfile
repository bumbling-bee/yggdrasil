FROM node:8

# make our app directory
RUN mkdir /yggdrasil

# set working directory to our app directory
WORKDIR /yggdrasil

# Update before installing dependencies
RUN apt-get update
RUN apt-get -y upgrade

# Get pip for Flask
RUN apt-get -y install python3-pip
RUN pip3 install --upgrade pip

# Copy over pip requirements
COPY requirements.txt /yggdrasil

# Install the python server library
RUN pip3 install -r /yggdrasil/requirements.txt

# Copy over npm requirements
COPY package.json /yggdrasil
COPY yarn.lock /yggdrasil

# Install dependencies
RUN yarn install

# Copy app directory
COPY . /yggdrasil

# # Build the resources
# RUN npm run build

# Set up environment variables
ENV FLASK_APP=/yggdrasil/run.py

# needed for flask to run with python3
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

# Run the image as a non-root user
RUN useradd -m bumblingbee
USER bumblingbee

CMD python3 run.py