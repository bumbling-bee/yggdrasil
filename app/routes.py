from app import app
from flask import render_template

@app.route("/")
def home():
    """Entry point into app"""
    return render_template('index.html')