const webpack = require('webpack');
const config = {
    entry: {
        home: __dirname + '/src/pages/home.jsx'
    },
    output: {
        path: __dirname + '/app/static/dist',
        filename: '[name].bundle.js'
    },
    resolve: {
        extensions: ['.js', '.jsx', '.vue'],
        alias: {
            vue: 'vue/dist/vue.min.js'
        }
    },
    module: {
        rules: [
            {
                test: /\.jsx?/,
                exclude: /node_modules/,
                use: 'babel-loader'
            },
            {
                test: /\.vue$/,
                exclude: /node_modules/,
                use: 'vue-loader'
            }
        ]
    }
};
module.exports = config;